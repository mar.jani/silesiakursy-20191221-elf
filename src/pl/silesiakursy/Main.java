package pl.silesiakursy;

import pl.silesiakursy.model.Elf;
import pl.silesiakursy.model.Gift;

import java.util.*;

public class Main {
    public static Integer christmasCoin = 10;
    private static List<Elf> elfs = new ArrayList<>();
    private static List<Gift> gifts = new ArrayList<>();
    private static Map<String, Integer> giftMap = new HashMap<>();

    private static Random random = new Random();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        elfs.add(new Elf("Legols", drawElfSkillLevel()));
        elfs.add(new Elf("Elrond", drawElfSkillLevel()));
        elfs.add(new Elf("Haldir", drawElfSkillLevel()));

        menu();
    }

    public static void menu() {
        System.out.println("---------------------------------");
        System.out.println("1. Dodaj Elfa");
        System.out.println("2. Wyświetl Elfy");
        System.out.println("3. Ulepsz Elfa");
        System.out.println("4. Wyprodukuj prezent");
        System.out.println("5. Lista wytworzonych prezentów");
        System.out.println("6. Wyświetl ilość ChristmasCoinów");
        System.out.println("0. Zakończ");
        System.out.println("---------------------------------");

        int indexMenu = 0;
        try {
            indexMenu = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e) {
            System.out.println("Błędna wartość");
        }

        if (indexMenu == 1) {
            createElf();
            menu();
        } else if (indexMenu == 2) {
            showElfList();
            menu();
        } else if (indexMenu == 3) {
            improveElf();
            menu();
        } else if (indexMenu == 4) {
            createGift();
            menu();
        } else if (indexMenu == 5) {
            showGiftsList();
            menu();
        } else if (indexMenu == 6) {
            showChristmasCoinAmount();
            menu();
        } else if (indexMenu == 0) {
            System.out.println("Do zobaczenia");
        }
    }

    private static void createElf() {
        System.out.println("Podaj imię elfa:");

        String name = null;
        try {
            name = scanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Elf elf = new Elf(name, drawElfSkillLevel());
        elfs.add(elf);
    }

    private static int drawElfSkillLevel() {
        if (random.nextInt(5) == 3) {
            return -3;
        }
        return 1;
    }

    private static void showElfList() {
        elfs.forEach(elf -> System.out.println(elf.toString()));
    }

    private static void improveElf() {
        System.out.println("Podaj UUID elfa do ulepszenia");

        String uuid = null;
        try {
            uuid = scanner.nextLine();
        } catch (Exception e) {
            System.out.println("Błędny identyfikator elfa");
        }

        for (Elf elf : elfs) {
            if (elf.getId().toString().equals(uuid)) {
                subtractChristmasCoin(elf);
                break;
            }
        }
    }

    private static void subtractChristmasCoin(Elf elf) {
        int elfLevel = elf.getLevel();

        if (christmasCoin >= 10 && elfLevel <= 4) {
            christmasCoin = -10;
            addPointLevelToElf(elf);
        } else if (christmasCoin >= 15 && elfLevel <= 10) {
            christmasCoin = -15;
            addPointLevelToElf(elf);
        } else {
            System.out.println("Niewystarczająca ilość ChristmasConis");
        }
    }

    private static void addPointLevelToElf(Elf elf) {
        if (random.nextInt(11) == 7) {
            elf.setLevel(elf.getLevel() + 3);
        } else {
            elf.setLevel(elf.getLevel() + 1);
        }
    }

    private static void createGift() {
        System.out.println("Podaj nazwę prezentu:");
        String giftName = null;
        try {
            giftName = scanner.nextLine();
        } catch (Exception e) {
            System.out.println("Błędna nazwa prezentu");
        }

        System.out.println("Podaj poziom trudności: ");
        int difficultyLevel = 0;
        try {
            difficultyLevel = scanner.nextInt();
            scanner.nextLine();
        } catch (Exception e) {
            System.out.println("Błędny poziom trudności");
        }

        int sumElfsLevels = getSumElfsLevels();
        if (random.nextInt(31) == 7) {
            sumElfsLevels *= 3;
        }

        if (sumElfsLevels >= difficultyLevel && !giftMap.containsKey(giftName)) {
            giftMap.put(giftName, difficultyLevel);
            christmasCoin += (difficultyLevel * 3);
        } else {
            System.out.println("Niewystarczający poziom umiejętności elfów\nlub prezent o podanej nazwie został już utworzony");
        }
    }

    private static int getSumElfsLevels() {
        int sumElfsLevels = 0;
        for (Elf elf : elfs) {
            sumElfsLevels += elf.getLevel();
        }
        return sumElfsLevels;
    }

    private static void showGiftsList() {
        for (Map.Entry<String, Integer> gift : giftMap.entrySet()) {
            String giftName = gift.getKey();
            Integer difficultyLevel = gift.getValue();
            System.out.println(giftName + ": " + difficultyLevel);
        }
    }

    private static void showChristmasCoinAmount() {
        System.out.println("Ilość ChristmasCoinów: " + christmasCoin);
    }
}
